# TABLETOP #

A virtual tabletop application written in node.js and React

### Installation ###

* From the API source directory, run `npm install` in console
* From the react-tabletop directory, run `npm install` in console
* To start the app, run `npm run start` from the API directory, or `npm start` from the react one.

### Dependencies ###
* Node.js
* MongoDB