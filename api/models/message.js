var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
    gameId: { type: mongoose.Schema.Types.ObjectId, ref: 'Game'},
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    body: String,
    dateCreated: Date
})

module.exports = mongoose.model('Message', MessageSchema);