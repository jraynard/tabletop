var mongoose = require('mongoose');

var GameSchema = new mongoose.Schema({
    name: String,
    description: String,
    gameMaster: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    players: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
})

module.exports = mongoose.model('Game', GameSchema);