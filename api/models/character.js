var mongoose = require('mongoose');

var CharacterSchema = new mongoose.Schema({
    gameId: { type: mongoose.Schema.Types.ObjectId, ref: 'Game'},
    name: String,
    crew: String,
    class: String,
    alias: String,
    look: String,
    heritage: String,
    background: String,
    vice: String,
    stress: Number,
    trauma: Number,
    traumaTypes: String,
    harmLevel3: String,
    harmLevel2: String,
    harmLevel1: String,
    healingClock: Number,
    armorNorma: Boolean,
    armorHeavy: Boolean,
    armorSpecial: Boolean,
    notes: String,
    stash: Number,
    coin: Number,
    playbookXP: Number,
    insightXP: Number,
    prowessXP: Number,
    resolveXP: Number,
    load: Number,
    readPermissions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    writePermissions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
})

module.exports = mongoose.model('Character', CharacterSchema);