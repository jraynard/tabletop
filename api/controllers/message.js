var Message = require('../models/message');

exports.getMessages = function(req, res) {
    Message.find( { gameId: req.params.gameId})
        .populate({ path: 'user', select: 'name'})
        .exec(function(err, messages){ 
            if (err) return res.send(err);

            res.json(messages);
    })
};

exports.postMessage = function(req, res) {
    var message = new Message(req.body);
    message.gameId = req.params.gameId;
    message.save(function(err) {
        if (err) return res.send(err);

        res.json(message);
    });
};

exports.deleteMessage = function(req, res) {
    Message.findOneAndRemove({ _id: req.params.messageId }, function(err, message){ 
        if (err) return res.send(err);

        res.json({ message: 'Message removed'});
    })
};