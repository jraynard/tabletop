var Character = require('../models/character');

exports.postCharacter = function(req, res){
    var character = new Character(req.body);
    character.gameId = req.params.gameId;
    character.save(function(err) {
        if (err) return res.send(err);

        res.json({ message: 'Character saved', data: character });
    });
};

exports.getCharactersByGame = function(req, res) {
    Character.find({ gameId: req.params.gameId }, function(err, characters){ 
        if (err) return res.send(err);

        res.json(characters);
    })
};

exports.getCharacter = function(req, res) {
    Character.findById(req.params.characterId, function(err, character){ 
        if (err) return res.send(err);

        res.json(character);
    })
};

exports.putCharacter = function(req, res) {
    Character.findOneAndUpdate({_id: req.params.characterId}, req.body, {new: true},  function(err, character){ 
        if (err) return res.send(err);

        res.json(character);
    })
};

exports.deleteCharacter = function(req, res) {
    Character.findByIdAndRemove(req.params.characterId, function(err, character){ 
        if (err) return res.send(err);

        res.json({ message: 'Character removed'});
    })
};