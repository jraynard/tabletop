var User = require('../models/user');

exports.postUser = function(req, res){
    var user = new User(req.body);

    user.save(function(err) {
        if (err) return res.send(err);

        res.json({ message: 'User saved', data: user });
    });
};

exports.getUser = function(req, res) {
    User.findOne( { _id: req.user._id }, function(err, user){ 
        if (err) return res.send(err);

        //remove the password field so it isn't returned
        user.password = undefined;

        res.json(user);
    })
};

exports.putUser = function(req, res) {
    User.findOneAndUpdate({ _id: req.user._id }, req.body, {new: true},  function(err, user){ 
        if (err) return res.send(err);

        res.json(user);
    })
};