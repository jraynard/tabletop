var Game = require('../models/game');

exports.postGame = function(req, res){
    var game = new Game(req.body);

    game.save(function(err) {
        if (err) return res.send(err);

        res.json({ message: 'Game saved', data: game });
    });
};

exports.getGames = function(req, res) {
    Game.find(function(err, games){ 
        if (err) return res.send(err);

        res.json(games);
    })
};

exports.getGame = function(req, res) {
    Game.findOne( { _id: req.params.gameId}, function(err, game){ 
        if (err) return res.send(err);

        res.json(game);
    })
};

exports.putGame = function(req, res) {
    Game.findOneAndUpdate({ _id: req.params.gameId}, req.body, {new: true},  function(err, game){ 
        if (err) return res.send(err);

        res.json(game);
    })
};

exports.deleteGame = function(req, res) {
    Game.findOneAndRemove({ _id: req.params.gameId }, function(err, game){ 
        if (err) return res.send(err);

        res.json({ message: 'Game removed'});
    })
};

exports.getMessageHistory = function(req, res) {
    Game.findOne( { _id: req.params.gameId}, function(err, game){ 
        if (err) return res.send(err);

        res.json(game.messageHistory);
    })
};

exports.putMessageHistory = function(req, res) {
    Game.findOne( { _id: req.params.gameId}, function(err, game){ 
        if (err) return res.send(err);

        game.messageHistory.push(req.body.message);
        game.save(function(err) {
            if (err) return res.send(err);
    
            res.json(game.messageHistory);
        });
    })
};

exports.deleteMessageHistory = function(req, res) {
    /* TODO */
};