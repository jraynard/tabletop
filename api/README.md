# TABLETOP #

An API for a virtual tabletop application written in node.js

### Installation ###

* From the source directory, run `npm install` in console
* To start the app, run `npm run start`

### Dependencies ###
* Node.js
* MongoDB