var characterController = require('./controllers/character');
var userController = require('./controllers/user');
var gameController = require('./controllers/game');
var messageController = require('./controllers/message');
var authController = require('./controllers/auth');

exports.initialize = function(router, app) {
    //create endpoints for specific character
    router.route('/character/:characterId')
        .get(characterController.getCharacter)
        .put(characterController.putCharacter)
        .delete(characterController.deleteCharacter);

    //create endpoints for users
    router.route('/user')
        .post(userController.postUser)
        .get(authController.isAuthenticated, userController.getUser)
        .put(userController.putUser)

    //create endpoints for games
    router.route('/game')
        .post(gameController.postGame)
        .get(gameController.getGames);

    //create endpoints for specific game
    router.route('/game/:gameId')
        .get(gameController.getGame)
        .put( gameController.putGame)
        .delete(gameController.deleteGame);

    router.route('/game/:gameId/characters')
        .post(characterController.postCharacter)
        .get(characterController.getCharactersByGame);

    router.route('/game/:gameId/messages')
        .get(messageController.getMessages)
        .post(messageController.postMessage)
    
    router.route('/game/:gameId/messages/:messageId')
        .delete(messageController.deleteMessage);

    app.use('/api', router);
}