import React from 'react';
import './game.css';
import Chat from './chat.js';

export default class Game extends React.Component {
    render() {
        return (
        <div className="game">
            <Chat id={this.props.id} />
        </div>
        );
    }
}