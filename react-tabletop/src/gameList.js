import React from 'react';
import './gameList.css';
import Game from './game.js';

export default class GameList extends React.Component {
    constructor() {
        super();
        this.state = {
            games: [],
            currentGame: null
        };
    }

    componentWillMount() {
        var headers = new Headers();
        /* TODO: Make this work for logged in user or whatever token based auth we use for the API */
        //headers.append("Authorization", "Basic " + base64.encode("justin.alvarez.raynard@gmail.com:test123"));
        fetch('http://localhost:3000/api/game/', {
            headers: headers
        })
        .then(results => {
            return results.json();
        })
        .then(data => {
            let games = data;
            this.setState({ games: games });
        })
    }

    handleClick(id) {
        if (id) {
            this.setState({
                games: this.state.games,
                currentGame: id
            });
        }
    }

    renderGame() {
        if (this.state.currentGame) {
            const currentGame = this.state.currentGame;
            return (
                <Game id={currentGame} />
            )
        }
    }

    render() {
        const games = this.state.games;
        let gameList = games.map((game) => {
            return(
                <li key={game._id} onClick={() => this.handleClick(game._id)}>{game.name}</li>
            )
        })

        return (
        <div>
            <div className="gameList">
                { gameList }
            </div>
            <div className="currentGame">
                { this.renderGame() }
            </div>
        </div>
        );
    }
}