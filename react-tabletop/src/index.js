import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import GameList from './gameList.js';
import registerServiceWorker from './registerServiceWorker';


  
// ========================================
  
ReactDOM.render(<GameList />, document.getElementById('root'));
registerServiceWorker();