import React from 'react';
import './chat.css';
const base64 = require('base-64');

function ChatMessage(props) {
    return (
    <li className="message">
        <span className="user">{props.userName}</span>: <span className="message">{ props.value }</span>
    </li>
    );
}

export default class Chat extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: []
        };
    }

    componentWillMount() {
        this.fetchMessages();
    }

    fetchMessages() {
        var headers = new Headers();
        /* TODO: Make this work for logged in user or whatever token based auth we use for the API */
        //headers.append("Authorization", "Basic " + base64.encode("justin.alvarez.raynard@gmail.com:test123"));
        fetch('http://localhost:3000/api/game/' + this.props.id + '/messages/', {
            headers: headers
        })
        .then(results => {
            return results.json();
        })
        .then(data => {
            let messages = data;
            this.setState({ messages: messages });
        })
    }

    handleSubmit(text) {
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        //headers.append("Authorization", "Basic " + base64.encode("justin.alvarez.raynard@gmail.com:test123"));
        let message = {
            user: "5b1eb60d633d3d0608f653da", /*TODO: Make this user dependent */
            body: text
        };
        var options = {
            method: 'POST',
            body: JSON.stringify(message ),
            headers: headers
        };
        fetch('http://localhost:3000/api/game/' + this.props.id + '/messages/', options)
        .then(() => { this.fetchMessages() })
    }

    render() {
        const messages = this.state.messages;
        let messageList = messages.map((message) => {
            return(
                <ChatMessage key={message._id} userName={message.user.name} value={message.body} />
            )
        })

        return (
            <div className="chat">
                <div className="chat-history">
                    <ul>
                        { messageList }
                    </ul>
                </div>
                <div className="chat-box">
                    <ChatForm onClick={(text) => this.handleSubmit(text)} />
                </div>
            </div>
        );
    }
}

class ChatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }
    handleSubmit(event) {
        this.props.onClick(this.state.value);
        event.preventDefault();
        this.setState({
            value: ''
        });
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                <input type="submit" value="Submit" />
            </form>
        )
    }
}